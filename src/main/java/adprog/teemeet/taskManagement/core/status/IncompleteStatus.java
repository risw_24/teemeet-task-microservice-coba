package adprog.teemeet.taskManagement.core.status;

public class IncompleteStatus implements Status {
    @Override
    public String showStatus() {
        return "Incomplete";
    }
}
