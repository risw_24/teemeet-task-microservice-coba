package adprog.teemeet.taskManagement.core.status;

public class InProgressStatus implements Status {
    @Override
    public String showStatus() {
        return "InProgress";
    }
}
