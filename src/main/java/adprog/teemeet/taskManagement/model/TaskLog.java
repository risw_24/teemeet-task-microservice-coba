package adprog.teemeet.taskManagement.model;
import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.core.status.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;

@ToString
@Entity
@Table(name = "tasklog")
@Data
@NoArgsConstructor
public class TaskLog {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="idTask")
    private Long idTask;

    @Column(name="judul")
    private String judulTask;

    @Column(name="percent")
    private int percent;

    @Column(name="status")
    private String status;

    @Column(name="assignne")
    private Long memberId;

    @Column(name="eventId")
    private Long eventId;

    @Column(name="groupId")
    private Long groupId;


/*
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="tasks")
    private TaskInAgenda tasks;


 */
    public TaskLog(String judulTask, Long memberId, long eventId, long groupId){
        this.judulTask = judulTask;
        this.percent = 0;
        this.memberId = memberId;
        this.status = "";
        this.eventId = eventId;
        this.groupId = groupId;
    }
}
