package adprog.teemeet.taskManagement.model;

import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.core.status.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@ToString
@Entity
@Table(name = "taskInAgenda")
@Data
@NoArgsConstructor
public class TaskInAgenda {

    @Id
    @Column(name="agendaId",updatable = false, nullable = false)
    private String agendaId;

    /*
    //@JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "tasks", cascade = CascadeType.ALL)
    private List<TaskLog> taskAgenda;

     */

    public TaskInAgenda(String agendaId){
        this.agendaId = agendaId;
    }
}
