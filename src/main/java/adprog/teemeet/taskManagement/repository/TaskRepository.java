package adprog.teemeet.taskManagement.repository;

import adprog.teemeet.taskManagement.model.TaskLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<TaskLog, Long> {
    List<TaskLog> findByIdTask(Long idTask);
    List<TaskLog> findTaskLogByEventId(Long eventId);
    List<TaskLog> findAllByOrderByIdTaskAsc();
    List<TaskLog> findTaskLogByGroupId(Long groupId);
    List<TaskLog> findTaskLogByMemberId(Long memberId);
}
