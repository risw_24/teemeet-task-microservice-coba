package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.TaskInAgendaRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import adprog.teemeet.taskManagement.service.TaskInAgendaService;
import adprog.teemeet.taskManagement.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(path = "/task")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskInAgendaService taskInAgendaService;

    @Autowired
    private TaskInAgendaRepository taskInAgendaRepository;

    @Autowired
    private TaskRepository taskRepository;

    @PostMapping(path = "", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createTask(@RequestParam(value="judulTask") String judulTask,
                                     @RequestParam(value="assignee") String assignne,
                                     @RequestParam(value="eventId") String eventId,
                                     @RequestParam(value="groupId") String groupId) {
        return ResponseEntity.ok(taskService.makeTask(judulTask,assignne,eventId,groupId));
    }

    @GetMapping(path = "", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getAllTasks() {
        return ResponseEntity.ok(taskService.getAllTasks());
    }

    // deprecated(?)
    @PostMapping(path = "/agenda", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createTaskInAgenda(@RequestParam(value="eventId") String eventId) {
        return ResponseEntity.ok(taskInAgendaService.create(eventId));
    }

    @GetMapping(path = "/taskId/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTask(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(taskService.getTask(id));
    }

    @GetMapping(path = "/eventId/{eventId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTaskByEvent(@PathVariable(value = "eventId") Long eventId) {
        return ResponseEntity.ok(taskService.getTaskByEventId(eventId));
    }

    @GetMapping(path = "/group/{groupId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTaskByGroup(@PathVariable(value = "groupId") Long groupId) {
        return ResponseEntity.ok(taskService.getTaskByGroupId(groupId));
    }

    @GetMapping(path="/member/{memberId}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTaskByMember(@PathVariable(value="memberId") Long memberId){
        return ResponseEntity.ok(taskService.getTaskByMemberId(memberId));
    }

    @DeleteMapping(path = "/taskId/{id}", produces = {"application/json"})
    public ResponseEntity deleteTask(@PathVariable(value = "id") Long id) {
        //taskService.removeTask(id);
        taskRepository.deleteById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(path = "/eventId/{eventId}", produces = {"application/json"})
    public ResponseEntity deleteTaskByEvent(@PathVariable(value = "eventId") Long eventId) {
        taskService.removeTaskByEvent(eventId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(path = "/groupId/{groupId}", produces = {"application/json"})
    public ResponseEntity deleteTaskByGroup(@PathVariable(value = "groupId") Long groupId) {
        taskService.removeTaskByGroup(groupId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(path = "/memberId/{memberId}", produces = {"application/json"})
    public ResponseEntity deleteTaskByMember(@PathVariable(value = "memberId") Long memberId) {
        taskService.removeTaskByMember(memberId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path="/{taskId}")
    public ResponseEntity updateTask(@PathVariable(value="taskId") long taskId, @RequestBody TaskLog taskLog){
        return ResponseEntity.ok(taskService.updateTask(taskId,taskLog));
    }

    @GetMapping(path="/test")
    public String test(){
        return "task/test";
    }
}
