package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.model.TaskLog;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskInAgendaService {
    TaskInAgenda getTaskInAgenda(String agendaId);
    TaskInAgenda create(String agenda);
    TaskInAgenda createWithTask(String agenda, TaskLog tasks);
    Iterable<TaskLog> getListTaskInAgenda(String agendaId);
}
