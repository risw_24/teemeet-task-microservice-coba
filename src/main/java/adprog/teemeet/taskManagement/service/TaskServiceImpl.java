package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.TaskInAgendaRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.Task;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService{
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskInAgendaRepository taskInAgendaRepository;

    @Autowired
    private TaskInAgendaService taskInAgendaService;

    @Override
    public TaskLog makeTask(String judulTask, String memberId, String eventId, String groupId){

        TaskLog taskLog = new TaskLog(judulTask, Long.parseLong(memberId), Long.parseLong(eventId), Long.parseLong(groupId));
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        taskLog.setStatus(incompleteStatus.showStatus());
        taskRepository.save(taskLog);

        return taskLog;
    }
    @Override
    public void removeTask(Long id){
        List<TaskLog> tasks = taskRepository.findByIdTask(id);
        for (TaskLog task : tasks) {
            taskRepository.delete(task);
        }
        //taskRepository.delete(task);
        /*
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupTaskService.deleteTaskGroup(group, task);
        if(taskRepository.findByIdTask(id).getAssignne().size()==0) {
            taskRepository.delete(task);
        }else{
            for(PersonLog assignee : task.getAssignne()){
                assignee.getTasks().remove(task);
            }
            taskRepository.delete(task);
        }
         */
    }

    @Override
    public void removeTaskByEvent(Long eventId) {
        List<TaskLog> tasks = taskRepository.findTaskLogByEventId(eventId);
        for (TaskLog task : tasks) {
            taskRepository.delete(task);
        }
    }

    @Override
    public void removeTaskByGroup(Long groupId) {
        List<TaskLog> tasks = taskRepository.findTaskLogByGroupId(groupId);
        for (TaskLog task : tasks) {
            taskRepository.delete(task);
        }
    }

    @Override
    public void removeTaskByMember(Long memberId) {
        List<TaskLog> tasks = taskRepository.findTaskLogByMemberId(memberId);
        for (TaskLog task : tasks) {
            taskRepository.delete(task);
        }
    }

    @Override
    public TaskLog updateTask(Long idTask, TaskLog task) {
        task.setIdTask(idTask);
        setStatusTask(task,task.getPercent());
        taskRepository.save(task);
        /*
        for(String numberId : assignne){
            PersonLog addPerson = personRepository.findByNumber(numberId);
            task.getAssignne().add(addPerson);
            addPerson.getTasks().add(task);
            personRepository.save(addPerson);
        }

         */
        return task;
    }

    public void setStatusTask(TaskLog task, int percent){
        CompleteStatus completeStatus = new CompleteStatus();
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        InProgressStatus inProgressStatus = new InProgressStatus();
        if(percent == 0){
            task.setStatus(incompleteStatus.showStatus());
        }else if(percent<100){
            task.setStatus(inProgressStatus.showStatus());
        }else{
            task.setStatus(completeStatus.showStatus());
        }
    }

    @Override
    public List<String> addAssignee(List<String> assignne) {
        List<String> addAssignne = assignne;
        return assignne;
    }

    @Override
    public List<TaskLog> getTaskByEventId(Long eventId) {
        return taskRepository.findTaskLogByEventId(eventId);
    }

    @Override
    public List<TaskLog> getTaskByGroupId(Long groupId){
        return taskRepository.findTaskLogByGroupId(groupId);
    }

    @Override
    public List<TaskLog> getAllTasks() {
        return taskRepository.findAllByOrderByIdTaskAsc();
    }
    /*
    @Override
    public Iterable<String> getAssignee(Long id) {
        TaskLog task = taskRepository.findByIdTask(id);
        return task.getAssignne();
    }

     */

    @Override
    public List<TaskLog> getTaskByMemberId(Long memberId) {
        return taskRepository.findTaskLogByMemberId(memberId);
    }

    @Override
    public List<TaskLog> getTask(Long id) {
        return taskRepository.findByIdTask(id);
    }
}
