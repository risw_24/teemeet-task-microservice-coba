package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.TaskInAgenda;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.TaskInAgendaRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskInAgendaServiceImpl implements TaskInAgendaService{
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private TaskInAgendaRepository taskInAgendaRepository;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskInAgendaService taskInAgendaService;

    @Override
    public TaskInAgenda getTaskInAgenda(String agendaId) {
        /*
        TaskInAgenda taskInAgenda = taskInAgendaRepository.findByAgendaId(agendaId);
        System.out.println("task milik agenda " + agendaId + ": " );
        for (TaskLog task : taskInAgenda.getTaskAgenda()){
            System.out.println(task.getJudulTask());
        }
        return taskInAgenda;
         */

        return null;
    }

    @Override
    public TaskInAgenda create(String agenda) {
        TaskInAgenda taskInAgenda = new TaskInAgenda(agenda);
        taskInAgendaRepository.save(taskInAgenda);
        return taskInAgenda;
    }
    @Override
    public TaskInAgenda createWithTask(String agenda, TaskLog tasks){
        /*
        TaskInAgenda taskInAgenda = taskInAgendaRepository.findByAgendaId(agenda);
        //List<TaskLog> listTask = taskInAgenda.getTaskAgenda();
        List<TaskLog> listTask = new ArrayList<>();
        TaskLog tasklog = taskRepository.findByIdTask(tasks.getIdTask());

        listTask.add(tasklog);
        taskInAgenda.setTaskAgenda(listTask);
        //tasklog.setTasks(taskInAgenda);

        taskInAgendaRepository.save(taskInAgenda);
        taskRepository.save(tasklog);

        System.out.println("hasil setelah save di createWith Task: " + taskInAgenda);
        return taskInAgenda;

         */
        return null;
    }

    @Override
    public Iterable<TaskLog> getListTaskInAgenda(String agendaId) {
        /*
        System.out.println("get list task agenda line 54 " + taskInAgendaRepository.findByAgendaId(agendaId));
        return taskInAgendaRepository.findByAgendaId(agendaId).getTaskAgenda();
        /*
        TaskInAgenda taskInAgenda = taskInAgendaRepository.findByAgendaId(agendaId);
        return taskInAgenda.getTaskAgenda();

         */
        /*
        TaskLog task = taskRepository.findByIdTask(id);
        return task.getAssignne();

         */
        return null;
    }
}
